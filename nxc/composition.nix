{ pkgs, setup, ... }: {

  nodes =
    let
      scripts = import ./my_scripts.nix { inherit pkgs setup; };
      common_config = { pkgs, ... }:
        {
          environment.etc = {
            ior_script = {
              text = import ./script_ior.nix { inherit pkgs setup; };
            };
          };
          networking.firewall.enable = false;
        };
    in
    {

      serverfs = { pkgs, ... }: {
        imports = [ common_config ./module.nix ];
        services.glusterfs.enable = true;
        services.my-startup = {
          enable = true;
          serverPaths = [ "serverfs:/data/gv0" ];
          volumeName = "gv0";
        };

        systemd.services.rpcbind = {
          wants = [ "systemd-tmpfiles-setup.service" ];
          after = [ "systemd-tmpfiles-setup.service" ];
        };
        systemd.tmpfiles.rules =
          [
            "d /data 0770 root root"
            "d /data/gv0 0770 root root"
          ];

      };

      client = { lib, ... }: {
        imports = [ common_config ];

        environment.systemPackages = with pkgs; [
          ior
          openmpi
          scripts.start_ior_nodes
          scripts.generate_ior_config
          glusterfs
        ];
        fileSystems =
          {
            "/gluster" =
              {
                device = "serverfs:/data/gv0";
                fsType = "glusterfs";
              };
          };

      };

    };
  testScript = ''
  '';
}
