{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.services.my-startup;
in
{

  ###### interface


  options = {
    services.my-startup = {
      enable = mkEnableOption "My startup ";

      package = mkOption {
        type = types.package;
        default = pkgs.glusterfs;
      };

      serverPaths = mkOption {
        # type = types.listOf;
        default = [ ];
      };

      volumeName = mkOption {
        # type = types.str;
        default = "gv0";
      };
    };
  };

  ###### implementation

  config = mkIf (cfg.enable) {
    systemd.services.my-startup = {
      description = "My startup";
      # wantedBy = [ "orangefs-server.service" ];
      after = [ "network.target" "glusterfs.service" ];
      serviceConfig.Type = "oneshot";
      script =
        ''
          ${cfg.package}/bin/gluster volume create ${cfg.volumeName} ${builtins.concatStringsSep " " cfg.serverPaths} force
          ${cfg.package}/bin/gluster volume start ${cfg.volumeName}
        '';
    };
  };
}
